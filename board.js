const firstStep = ['■ □ ■ □ ■ □ ■ □'];
const secondStep = ['□ ■ □ ■ □ ■ □ ■'];

for (let i = 0; i < 8; i++) {
    if (i % 2 === 0) {
        console.log(firstStep);
    } else {
        console.log(secondStep);
    }
}